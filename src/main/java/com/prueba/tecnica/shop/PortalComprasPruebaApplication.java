package com.prueba.tecnica.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalComprasPruebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortalComprasPruebaApplication.class, args);
    }

}
