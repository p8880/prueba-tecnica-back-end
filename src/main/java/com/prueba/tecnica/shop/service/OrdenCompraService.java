package com.prueba.tecnica.shop.service;

import com.prueba.tecnica.shop.model.dto.DtoRequest;
import com.prueba.tecnica.shop.model.entities.OrdenCompraEntity;

import java.util.List;

public interface OrdenCompraService {
    public void saveOrUpdate(DtoRequest ordenCompraEntity);
    public List<OrdenCompraEntity> findAll();
    public void delete(Long id);
}
