package com.prueba.tecnica.shop.service;

import com.prueba.tecnica.shop.model.entities.ProductoEntity;

import java.util.List;

public interface ProductoService {
    public void saveOrUpdate(ProductoEntity productoEntity);
    public List<ProductoEntity> findAll();
    public void delete(Long id);
}
