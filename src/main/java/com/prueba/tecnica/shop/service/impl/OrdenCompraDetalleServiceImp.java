package com.prueba.tecnica.shop.service.impl;

import com.prueba.tecnica.shop.model.entities.OrdenCompraDetalleEntity;
import com.prueba.tecnica.shop.repository.OrdenCompraDetalleRepository;
import com.prueba.tecnica.shop.service.OrdenCompraDetalleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrdenCompraDetalleServiceImp implements OrdenCompraDetalleService {

    @Autowired
    private OrdenCompraDetalleRepository ordenCompraDetalleRepository;

    @Override
    public void saveOrUpdate(OrdenCompraDetalleEntity ordenCompraDetalleEntity) {
        ordenCompraDetalleRepository.save(ordenCompraDetalleEntity);
        ordenCompraDetalleRepository.flush();
    }

    @Override
    public List<OrdenCompraDetalleEntity> findAll() {
        return ordenCompraDetalleRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        ordenCompraDetalleRepository.deleteById(id);
    }
}
