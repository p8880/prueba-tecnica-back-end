package com.prueba.tecnica.shop.service;

import com.prueba.tecnica.shop.model.entities.OrdenCompraDetalleEntity;

import java.util.List;

public interface OrdenCompraDetalleService {
    public void saveOrUpdate(OrdenCompraDetalleEntity ordenCompraDetalleEntity);
    public List<OrdenCompraDetalleEntity> findAll();
    public void delete(Long id);
}
