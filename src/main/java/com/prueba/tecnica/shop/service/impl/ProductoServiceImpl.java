package com.prueba.tecnica.shop.service.impl;

import com.prueba.tecnica.shop.model.entities.ProductoEntity;
import com.prueba.tecnica.shop.repository.ProductoRepository;
import com.prueba.tecnica.shop.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    @Override
    public void saveOrUpdate(ProductoEntity productoEntity) {
        productoRepository.save(productoEntity);
        productoRepository.flush();
    }

    @Override
    public List<ProductoEntity> findAll() {
        return productoRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        productoRepository.deleteById(id);
    }
}
