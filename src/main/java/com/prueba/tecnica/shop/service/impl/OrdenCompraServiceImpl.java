package com.prueba.tecnica.shop.service.impl;

import com.prueba.tecnica.shop.model.dto.DtoRequest;
import com.prueba.tecnica.shop.model.entities.OrdenCompraDetalleEntity;
import com.prueba.tecnica.shop.model.entities.OrdenCompraEntity;
import com.prueba.tecnica.shop.repository.OrdenCompraRepository;
import com.prueba.tecnica.shop.service.OrdenCompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OrdenCompraServiceImpl implements OrdenCompraService {

    @Autowired
    private OrdenCompraRepository OrdenCompraService;

    @Autowired
    private OrdenCompraDetalleServiceImp ordenCompraDetalleServiceImp;

    @Override
    public void saveOrUpdate(DtoRequest dtoRequest) {
        OrdenCompraEntity ordenCompraEntity = dtoRequest.getOrdenCompraEntity();
        ordenCompraEntity.setFechaIngreso(new Date());
        ordenCompraEntity.setUsuarioIngresa("gramirez");
        OrdenCompraService.save(ordenCompraEntity);
        OrdenCompraService.flush();

        for(OrdenCompraDetalleEntity ordenCompraDetalleEntity : dtoRequest.getDetalleEntity()){
            ordenCompraDetalleEntity.setFechaIngreso(new Date());
            ordenCompraDetalleEntity.setUsuarioIgreso("gramirez");
            ordenCompraDetalleEntity.setIdOrdenCompra(ordenCompraEntity.getId());
            ordenCompraDetalleServiceImp.saveOrUpdate(ordenCompraDetalleEntity);
        }

    }

    @Override
    public List<OrdenCompraEntity> findAll() {
        return OrdenCompraService.findAll();
    }

    @Override
    public void delete(Long id) {
        OrdenCompraService.deleteById(id);
    }
}
