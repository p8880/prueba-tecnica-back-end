package com.prueba.tecnica.shop.controller;

import com.prueba.tecnica.shop.model.dto.DtoRequest;
import com.prueba.tecnica.shop.model.dto.DtoResponse;
import com.prueba.tecnica.shop.service.impl.OrdenCompraServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/compras")
public class ComprasController {

    @Autowired
    private OrdenCompraServiceImpl ordenCompraService;

    @PostMapping(value = "/crear")
    public @ResponseBody
    DtoResponse crear(@RequestBody DtoRequest dtoRequest){
        try{
            ordenCompraService.saveOrUpdate(dtoRequest);
        }catch (Exception exception){
            return new DtoResponse(2L,"505","Hubo un error en el servidor!",null);
        }
        return new DtoResponse(1L,"202","Guardado Exitosamente!",null);
    }

    @GetMapping(value = "/findAll")
    public @ResponseBody
    DtoResponse findAll(){
        Object obj = null;
        try{
             obj = ordenCompraService.findAll();
        }catch (Exception exception){
            return new DtoResponse(2L,"505","Hubo un error en el servidor!", null);
        }
        return new DtoResponse(1L,"202","Registros recuperados Exitosamente!", obj);
    }

    @GetMapping(value = "/eliminar/{id}")
    public @ResponseBody
    DtoResponse eliminar(@PathVariable Long id){
        try{
            ordenCompraService.delete(id);
        }catch (Exception exception){
            return new DtoResponse(2L,"505","Hubo un error en el servidor!",null);

        }
        return new DtoResponse(1L,"202","Eliminado Exitosamente!",null);
    }
}
