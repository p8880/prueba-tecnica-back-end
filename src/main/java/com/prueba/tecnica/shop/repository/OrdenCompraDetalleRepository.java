package com.prueba.tecnica.shop.repository;

import com.prueba.tecnica.shop.model.entities.OrdenCompraDetalleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdenCompraDetalleRepository extends JpaRepository<OrdenCompraDetalleEntity,Long> {
}
