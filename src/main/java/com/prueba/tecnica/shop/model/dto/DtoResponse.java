package com.prueba.tecnica.shop.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DtoResponse {
    public Long id;
    public String codeDetail;
    public String responseDetail;
    public Object body;
}
