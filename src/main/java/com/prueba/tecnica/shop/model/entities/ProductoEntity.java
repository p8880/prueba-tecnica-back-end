package com.prueba.tecnica.shop.model.entities;

import javax.persistence.*;
import java.util.Date;

@Table(name="t_producto", schema = "logistic_main")
@Entity
public class ProductoEntity {
    private Long id;
    private String nombre;
    private Date fechaIngreso;
    private String usuarioIngreso;

    @Id
    @SequenceGenerator(name = "sequence-producto-generator", sequenceName = "t_producto_id_seq", allocationSize = 1, schema = "logistic_main")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-producto-generator")
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "fecha_ingreso", nullable = false)
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Column(name = "usuario_ingreso", nullable = false)
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }

    public void setUsuarioIngreso(String usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }
}
