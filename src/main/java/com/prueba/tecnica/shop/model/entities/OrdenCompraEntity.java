package com.prueba.tecnica.shop.model.entities;

import javax.persistence.*;
import java.util.Date;

@Table(name="t_orden_compra", schema = "logistic_main")
@Entity
public class OrdenCompraEntity {
    private Long id;
    private String nombreCliente;
    private String tipoFacturacion;
    private Double total_pagar;
    private Date fechaIngreso;
    private String usuarioIngresa;

    @Id
    @SequenceGenerator(name = "sequence-orden-compra-generator", sequenceName = "t_orden_compra_id_seq", allocationSize = 1, schema = "logistic_main")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-orden-compra-generator")
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "nombre_cliente", nullable = false, length = 100)
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    @Column(name = "tipo_factura", nullable = false, length = 20)
    public String getTipoFacturacion() {
        return tipoFacturacion;
    }

    public void setTipoFacturacion(String tipoFacturacion) {
        this.tipoFacturacion = tipoFacturacion;
    }

    @Column(name = "total_pagar", nullable = false)
    public Double getTotal_pagar() {
        return total_pagar;
    }

    public void setTotal_pagar(Double total_pagar) {
        this.total_pagar = total_pagar;
    }

    @Column(name = "fecha_ingreso", nullable = false)
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Column(name = "usuario_ingresa", nullable = false, length = 100)
    public String getUsuarioIngresa() {
        return usuarioIngresa;
    }

    public void setUsuarioIngresa(String usuarioIngresa) {
        this.usuarioIngresa = usuarioIngresa;
    }
}
