package com.prueba.tecnica.shop.model.dto;

import com.prueba.tecnica.shop.model.entities.OrdenCompraDetalleEntity;
import com.prueba.tecnica.shop.model.entities.OrdenCompraEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DtoRequest {
    public Long id;
    public String codeDetail;
    public String resquestDetail;
    public OrdenCompraEntity ordenCompraEntity;
    public List<OrdenCompraDetalleEntity> detalleEntity;
}
