package com.prueba.tecnica.shop.model.entities;

import javax.persistence.*;
import java.util.Date;

@Table(name="t_det_orden_compra", schema = "logistic_main")
@Entity
public class OrdenCompraDetalleEntity {


    private Long id;
    private Long idOrdenCompra;
    private Long idProducto;
    private int cantidad;
    private String usuarioIgreso;
    private Date fechaIngreso;

    @Id
    @SequenceGenerator(name = "sequence-detalle-orden-compra-generator", sequenceName = "t_det_orden_compra_id_seq", allocationSize = 1, schema = "logistic_main")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-detalle-orden-compra-generator")
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column(name = "id_orden_compra", nullable = false)
    public Long getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Long idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    @Column(name = "id_producto", nullable = false)
    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    @Column(name = "cantidad", nullable = false)
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Column(name = "usuario_ingreso", nullable = false, length = 100)
    public String getUsuarioIgreso() {
        return usuarioIgreso;
    }

    public void setUsuarioIgreso(String usuarioIgreso) {
        this.usuarioIgreso = usuarioIgreso;
    }

    @Column(name = "fecha_Ingreso", nullable = false)
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}
