create or replace procedure actualiza_precio_producto (id_producto number, precio_producto decimal) is begin
    update t_prodcto set precio=precio_producto where id=id_producto;
    commit;
end actualiza_precio_producto;